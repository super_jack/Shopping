# day 01

# ## users


# day 02

# ## verifications
# 1/ 图片验证码
# 2/ 短信验证码
# 3/ 注册登陆验证


# day 03

# ## oauth
# 1/ 第三方登陆 (QQ)


# day 04

# ## areas
# 1/ 用户中心信息显示
# 2/ 邮件发送流程及其验证链接
# 3/ 用户地址 行政区划自关联保存与显示
# 4/ 用户地址管理 默认地址的增删改查


# day 05

# ## ads
# 1/ 数据表分析: 先有数据表，理清表与表之间的关系，再生成数据模型
# 2/ 页面静态化，设置定时任务，实现生成首页静态化页面并返回
# 3/ 使用 docker 部署 Tracker 和 Storage，并搭建 FastDFS 分布式存储系统
# 4/ docker，FastDFS，CKEditor 相关配置


# day 06

# ## goods
# 1/ 商品详情页的页面静态化处理保存
# 2/ 使用 Django-admin 模型类管理器 触发 celery 异步任务，实现商品详情页的静态化页面保存
# 3/ 编写 python 脚本 调用生成详情页静态化页面
# 4/ 查询和保存用户浏览历史记录


# day 07

# ## goods
# 1/ 商品列表页分页展示
# 2/ 搜索页面功能
# 3/ haystack 对接 elasticsearch 搜索引擎


# day 08
# ## cart
# 1/ 购物车的增删改查操作
# 2/ 全选购物车数据
# 3/ 用户登陆以及第三方登陆时合并购物车数据


# day 09
# ## orders
# 1/ 提交并保存订单
# 2/ 数据库事务      @transaction.atomic     with transaction.atomic()       transaction.savepoint_rollback()



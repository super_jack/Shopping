#! /usr/bin/env python

# sys.path 中尽量使用绝对路径，脚本当前位置需要和配置文件设置为同级目录
import sys
sys.path.insert(0, '../')

# 由于 python 脚本独立与 Django，执行操作时，又依赖于 Django 程序
# 所以需要在脚本中提供 Django 的环境依赖，同 celery 异步任务

# 加载 Django 配置文件到独立脚本
import os
if not os.getenv('DJANGO_SITTINGS_MODULE'):
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shopping.settings.dev")

# 让 Django 初始化
import django
django.setup()

from ads.crons import generate_static_index_html


if __name__ == '__main__':
    generate_static_index_html()
from celery import Celery

# 为 celery 提供 Django 默认配置
# 为 celery 提供 Django 依赖
import os
if not os.getenv('DJANGO_SITTINGS_MODULE'):
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shopping.settings.dev")

# 创建 celery 对象
celery_app = Celery('shopping')

# 导入 celery 配置文件
celery_app.config_from_object('celery_tasks.config')

# 导入 task 任务
celery_app.autodiscover_tasks(['celery_tasks.sms', 'celery_tasks.email', 'celery_tasks.html'])

# 运行 celery 异步任务
# celery -A celery_tasks.main worker -l info

# 更新 kombu 和 celery 版本
# pip install kombu==4.2.0
# pip install celery==4.1.1
# 解决 broker 任务队列 redis 版本过低但问题


# django 使用 delay 调用任务
# config: 添加 redis 配置，充当 broker 任务队列
# tasks: 编写具体耗时任务，并将 celery 对象装饰到任务函数上
# main: 创建 celery 对象，加载 broker 配置，以及 task 任务

from shopping.celery_tasks.main import celery_app
from django.core.mail import send_mail
from django.conf import settings


@celery_app.task(name="send_active_email")
def send_active_email(to_email, verify_url):
    subject = 'shopping e-mail validated!'
    html_message = '<p>尊敬的用户你好！</p>' \
                    '<p>感谢你使用 shopping 购物网站</p>' \
                    '<p>您的邮箱为：%s，请点击此链接激活您的邮箱</p>' \
                    '<p><a href="%s">%s<a></p>' % (to_email, verify_url, verify_url)

    send_mail(subject, "", settings.EMAIL_FROM, [to_email], html_message=html_message)
from rest_framework.pagination import PageNumberPagination


class LargeResultsSetPagination(PageNumberPagination):
    """自定义分页器"""
    page_size = 1000
    page_size_query_param = 'page_size'
    max_page_size = 10000
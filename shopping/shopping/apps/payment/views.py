from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from orders.models import OrderInfo
from alipay import AliPay
from django.conf import settings
import os

# Create your views here.


# GET /orders/(?P<order_id>\d+)/payment/
from payment.models import Payment


class PaymentView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, order_id):
        """
        获取支付链接
        :param order_id: 订单编号
        :return: alipay_url：支付宝链接
        """
        user = request.user
        # 获取参数校验
        try:
            order = OrderInfo.objects.get(
                order_id=order_id,
                user=user,
                status=OrderInfo.ORDER_STATUS_ENUM['UNPAID'],
                pay_method=OrderInfo.PAY_METHODS_ENUM['ALIPAY'],
            )
        except OrderInfo.DoesNotExist:
            return Response({'message': '订单信息有误'}, status=status.HTTP_400_BAD_REQUEST)

        # 向支付宝发起请求
        alipay_client = AliPay(
            appid=settings.ALIPAY_APPID,
            app_notify_url=None,  # 默认回调url
            app_private_key_string=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'keys/app_private_key.pem'),
            alipay_public_key_string=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'keys/alipay_public_key.pem'),
            sign_type="RSA2" ,
            debug = settings.ALIPAY_DEBUG
        )

        # 电脑网站支付 需要跳转到 https://openapi.alipay.com/gateway.do? + order_string
        order_string = alipay_client.api_alipay_trade_page_pay(
            out_trade_no=order_id,
            total_amount=order.total_amount,
            subject='shopping 电商购物网站订单是%s' % order_id,
            return_url="http://www.shopping.site:8080/pay_success.html",
            notify_url=None
        )

        # 拼接支付链接网址
        alipay_url = settings.ALIPAY_URL + order_string

        # 返回
        return  Response({'alipay_url': alipay_url})


class PaymentStatusView(APIView):
    """
    刷新支付状态信息
    """
    def put(self, request):
        # 获取参数
        alipay_req_data = request.query_params  # QueryDict
        if not alipay_req_data:
            return Response({'message': '缺少参数'}, status=status.HTTP_400_BAD_REQUEST)

        alipay_req_dict = alipay_req_data.dict()
        sign = alipay_req_dict.pop('sign')

        # 向支付宝发起请求
        alipay_client = AliPay(
            appid=settings.ALIPAY_APPID,
            app_notify_url=None,  # 默认回调url
            app_private_key_string=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'keys/app_private_key.pem'),
            alipay_public_key_string=os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                                  'keys/alipay_public_key.pem'),
            sign_type="RSA2",
            debug=settings.ALIPAY_DEBUG
        )

        # 返回验证结果
        result = alipay_client.verify(alipay_req_dict, sign)

        if result:
            order_id = alipay_req_dict.get('out_trade_no')
            trade_id = alipay_req_dict.get('trade_no')

            # 保存支付结果数据
            Payment.objects.create(
                order_id=order_id,
                trade_id=trade_id
            )

            # 修改订单状态
            OrderInfo.objects.filter(order_id=order_id).update(status=OrderInfo.ORDER_STATUS_CHOICES['UNSEND'])
            return Response({'trade_id': trade_id})
        else:
            return Response({'message': '参数错误'}, status=status.HTTP_400_BAD_REQUEST)

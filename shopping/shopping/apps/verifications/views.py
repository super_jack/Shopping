import random
import logging
from django.http import HttpResponse

from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from shopping.shopping.libs.captcha.captcha import captcha
from django_redis import get_redis_connection

from verifications.seriallizers import ImageCodeCheckSerializer
from . import constants
from shopping.celery_tasks.sms.tasks import send_sms_code

# Create your views here.

# 分析业务逻辑，明确这个业务中需要涉及的相关子业务，针对子业务设计该接口
# 分析接口功能任务，明确接口访问方式和返回数据
# 1/ 接口请求方式
# 2/ 接口 URL 路径定义
# 3/ 需要前端传递的数据和数据格式（路径参数，查询字符串，请求表单或 JSON 等）
# 4/ 返回给前端的数据及数据格式

logger = logging.getLogger('django')


# 图片验证码
# GET /image_codes/(?P<image_code_id>[\w-]+)/
# 请求参数: uuid 字符串
# 返回数据: 图片验证码
class ImageCodeView(APIView):
    """图片验证码"""
    def get(self, request, image_code_id):

        # 生成验证码图片
        text, image = captcha.generate_captcha()

        # 保存真实值
        redis_conn = get_redis_connection('verify_codes')
        redis_conn.setex('img_%s' % image_code_id, constants.IMAGE_CODE_REDIS_EXPIRES, text)

        # 返回前端
        return HttpResponse(image, content_type='image/jpg')


# GET /sms_codes/(?P<mobile>1[3-9]\d{9})/
class SMSCodeView(GenericAPIView):
    """
    短信验证码
    传入参数: mobile, image_code_id, text
    """
    serializer_class = ImageCodeCheckSerializer

    def get(self, request, mobile):
        # 校验参数 使用序列化器完成 ImageCode
        serializer = self.get_serializer(data=request.query_params)     # 校验查询字符集参数
        serializer.is_volid(raise_exception=True)

        # 生成短信验证码
        sms_code = '%06d' % random.randint(6, 999999)

        # 保存短信验证码，发送记录
        redis_conn = get_redis_connection('verify_codes')
        # redis_conn.setex('sms_%s' % mobile, constants.SMS_CODE_REDIS_EXPIRES, sms_code)
        # redis_conn.setex('sms_flag_%s' % mobile, constants.SEND_SMS_CODE_INTERVAL, 1)

        # redis 管道
        pl = redis_conn.pipeline()
        pl.setex('sms_%s' % mobile, constants.SMS_CODE_REDIS_EXPIRES, sms_code)
        pl.setex('sms_flag_%s' % mobile, constants.SEND_SMS_CODE_INTERVAL, 1)
        pl.execute()        # 减少网络通讯次数，多次设置，一次执行

        # 使用 celery 发送短信并返回
        expires = constants.SMS_CODE_REDIS_EXPIRES // 60
        temp_id = constants.SMS_CODE_TEMP_ID

        send_sms_code.delay(mobile, sms_code, expires, temp_id)

        return Response({'message': 'ok'})




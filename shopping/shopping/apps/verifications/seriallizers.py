from django_redis import get_redis_connection
from redis import RedisError
from rest_framework import serializers


class ImageCodeCheckSerializer(serializers.ModelSerializer):
    """
    图形验证码验证
    """
    image_code_id = serializers.UUIDField()
    text = serializers.CharField(max_length=4, min_length=4)

    def validate(self, attrs):
        # 取出校验数据
        image_code_id = attrs['imag_code_id']
        text = attrs['text']

        redis_conn = get_redis_connection('verify_codes')
        real_image_code_text = redis_conn.get('img_%s' % image_code_id)

        if not real_image_code_text:
            raise serializers.ValidationError('图片验证码无效')

        try:
            redis_conn.delete('img_%s' % image_code_id)
        except RedisError as e:
            print(e)

        # 比较真实值
        real_image_code_text = real_image_code_text.decode()
        if real_image_code_text.lower() != text.lower():
            raise serializers.ValidationError('图片验证码错误')

        # 60s 之内是否发送

        # ## get_serializer() 创建序列化对象时，会补充 context 属性，
        # ## context 属性包含3个值 request，format，view

        # ## django 类视图对象中，kwargs 属性保存了路径提取出来但参数
        mobile = self.context['view'].kwargs['mobile']

        # 此时 send_flag 未过期，不能重复发送
        send_flag = redis_conn.get('send_flag_%s' % mobile)
        if send_flag:
            raise serializers.ValidationError('请求次数过于频繁')

        return attrs
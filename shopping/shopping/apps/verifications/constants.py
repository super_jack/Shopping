# 图片验证码 redis 有效期 单位: 秒
IMAGE_CODE_REDIS_EXPIRES = 300

# 短信验证码 redis 有效期 单位: 秒
SMS_CODE_REDIS_EXPIRES = 300

# 短信验证码 发送间隔
SEND_SMS_CODE_INTERVAL = 60

SMS_CODE_TEMP_ID = 1
from django.shortcuts import render

# Create your views here.
# 添加 redis 缓存
from rest_framework.pagination import LimitOffsetPagination
from rest_framework_extensions.cache.decorators import cache_response
from rest_framework_extensions.cache.mixins import CacheResponseMixin
from areas.models import Area
from rest_framework import generics
from rest_framework import mixins
from areas import serializers
from rest_framework.viewsets import GenericViewSet, ReadOnlyModelViewSet
#
# -------------------------------------------------------------------------------------------------------------------- #
# # GET /areas/
# class AreasView(generics.ListAPIView):
#     pass
#
# # GET /areas/<pk>/
# class SubAreasView(generics.RetrieveAPIView):
#     pass
# -------------------------------------------------------------------------------------------------------------------- #
# class AreasViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, GenericViewSet):
#     pass
# -------------------------------------------------------------------------------------------------------------------- #
# @action   分发路由
# /areas/           {'get': 'list'}
# /areas/<pk>/      {'get': 'retrieve'}
# -------------------------------------------------------------------------------------------------------------------- #
# 使用 redis 缓存   针对类视图(视图集)，只能在返回数据时使用缓存，数据的增删改查不需要使用缓存处理
# 1/ 装饰器: @cache_response(cache='redis 库 default', timeout='expire_in')；
# 2/ 缓存扩展类: BaseCacheResponseMixin, RetrieveCacheResponseMixin, ListCacheResponseMixin, CacheResponseMixin
# 配合 RetrieveModelMixin 和 ListModelMixin 类视图扩展类使用。
# -------------------------------------------------------------------------------------------------------------------- #
#


class AreasViewSet(CacheResponseMixin, ReadOnlyModelViewSet):
    pagination_class = LimitOffsetPagination            # None  关闭分页

    # 改写 queryset
    def get_queryset(self):                         # 改写 get_queryset 方法，获取不同 action 的 queryset
        if self.action == 'list':
            return Area.objects.filter(parent=None)
        else:
            return Area.objects.all()

    # 改写 serializer
    def get_serializer_class(self):
        if self.action == 'list':
            return serializers.AreaSerializer
        else:
            return serializers.SubAreaSerializer

from rest_framework import serializers

from areas.models import Area


class AreaSerializer(serializers.ModelSerializer):
    """返回行政区划序列化器"""
    class Meta:
        model = Area
        fields = ('id', 'name')

    # {
    #     'id': xxx,
    #     'name': xxx
    # },


class SubAreaSerializer(serializers.ModelSerializer):
    """嵌套关联行政区划对象序列化器"""

    # 处理 subs 子集时，使用 AreaSerializer 序列化器
    subs = AreaSerializer(many=True, read_only=True)      # 处理多个子集对象 many=True; 单独校验，只做数据返回 read_only=True

    class Meta:
        model = Area
        fields = ('id', 'name', 'subs')

    # {
    #     'id': xxx,
    #     'name': xxx,
    #     'subs': [
    # --------------------------------
    #         {
    #             'id': xxx,
    #             'name': xxx
    #         },
    # --------------------------------
    #         {
    #             'id': xxx,
    #             'name': xxx
    #         },
    # --------------------------------
    #               .....
    # --------------------------------
    #         {
    #             'id': xxx,
    #             'name': xxx
    #         }
    # --------------------------------
    #     ]
    # }

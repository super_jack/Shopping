from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from . import views


urlpatterns = [

]

# @action   分发路由
# /areas/           {'get': 'list'}
# /areas/<pk>/      {'get': 'retrieve'}

route = DefaultRouter()
route.register('areas', views.AreasViewSet, base_name='areas')
urlpatterns += route.urls

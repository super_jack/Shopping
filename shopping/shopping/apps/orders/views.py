from decimal import Decimal

from django.shortcuts import render

# Create your views here.
from django_redis import get_redis_connection
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics
from . import serializers
from goods.models import SKU


class OrderSettlementView(APIView):
    """
    提交订单结算
    """
    permission_classes = [IsAuthenticated]

    def get(self, request):
        # 获取用户对象    user
        user = request.user

        # 从 redis 中查询购物车: sku_id, count, selected
        redis_conn = get_redis_connection('cart')
        pl = redis_conn.pipeline()
        # hash, set: count, selected
        redis_cart_dict = pl.hgetall('cart_%s' % user.id)
        redis_cart_selected = pl.smembers('cart_selected_%s' % user.id)
        pl.execute()

        cart = {}
        for sku_id in redis_cart_selected:
            cart[int(sku_id)] = int(redis_cart_dict[sku_id])

        # 查询数据库
        sku_id_list = cart.keys()
        sku_obj_list = SKU.objects.filter(id__in=sku_id_list)

        for sku in sku_obj_list:
            sku.count = cart[sku.id]

        # 运费
        freight = Decimal('10.00')

        # 序列化返回
        # 1.
        # serializer = serializers.CartSKUSerializer(sku_obj_list, many=True)
        # return Response({'freight': freight, 'skus': serializer.data})

        # 2.
        serializer = serializers.OderSettlementSerializer({'freight': freight, 'skus': sku_obj_list})
        return Response(serializer.data)


# POST /orders/
# params: address int, pay_method int   json form
# return: order_id char     json

class SaveOrderView(generics.CreateAPIView):
    """
    保存订单
    """
    serializer_class = serializers.SaveOrderSerializer
    permission_classes = [IsAuthenticated]

























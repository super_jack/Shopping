from decimal import Decimal
from django.utils import timezone
from django_redis import get_redis_connection
from rest_framework import serializers
from django.db import transaction
from goods.models import SKU
from orders.models import OrderInfo, OrderGoods
import logging


logger = logging.getLogger('django')


class CartSKUSerializer(serializers.ModelSerializer):
    """
    购物车商品数据序列化器
    """
    count = serializers.IntegerField(label='数量')

    class Meta:
        model = SKU
        fields = ('id', 'name', 'default_image_url', 'price', 'count')


# 嵌套序列化器
class OderSettlementSerializer(serializers.Serializer):
    """
    订单结算序列化器
    """
    freight = serializers.DecimalField(label='运费', max_digits=10, decimal_places=2)
    skus = CartSKUSerializer(many=True)

"""
{
    'freight': '10.00',
    'skus': [
        {
            'id': 10,
            'name': 'huawei mate20 pro',
            'default_image_url': 'http://image.shopping.site/1/',
            'price': '3700.00',
            'count': 1
        },
        {
            'id': 16,
            'name': 'macbook pro',
            'default_image_url': 'http://image.shopping.site/2/',
            'price': '19500',
            'count': 1
        }
    ]
}
"""


class SaveOrderSerializer(serializers.ModelSerializer):
    """
    保存订单序列化器
    """
    class Meta:
        model = OrderInfo
        fields = ('address', 'pay_method', 'order_id')

        read_only_fields = ('order_id',)
        extra_kwargs = {
            'address': {
                'read_only': True
            },
            'pay_method': {
                'required': True
            }
        }

    def create(self, validated_data):
        """保存订单"""
        address = validated_data['address']
        pay_method = validated_data['pay_method']

        # 获取用户对象 user
        user = self.context['request'].user

        # 查询购物车 redis: sku_id, count, selected
        redis_conn = get_redis_connection('cart')
        redis_cart_dict = redis_conn.hgetall('cart_%s' % user.id)
        redis_cart_selected = redis_conn.smembers('cart_selected_%s' % user.id)

        cart = {}
        for sku_id in redis_cart_selected:
            cart[int(sku_id)] = int(redis_cart_dict[sku_id])

        if not cart:
            raise serializers.ValidationError('没有需要结算的商品')

        try:
            # 创建事务
            with transaction.atomic():
                # 创建保存点
                save_id = transaction.savepoint()

                # 保存订单，生成 order_id
                order_id = timezone.now().strftime('%Y:%m:%d %H:%M:%S') + ('%06d' % user.id)

                # 创建订单基本信息记录 OrderInfo
                order = OrderInfo.objects.create(
                    order_id=order_id,
                    user=user,
                    address=address,
                    total_count=0,
                    total_amount=Decimal('0'),
                    freight=Decimal('10.00'),
                    pay_method=pay_method,
                    status=OrderInfo.PAY_METHODS_ENUM['UNSEND'] if pay_method else None
                )

                # 查询商品数据库，获取商品数据 库存
                sku_id_list = cart.keys()
                # sku_object_list = SKU.objects.filter(id__in=sku_id_list)

                # 遍历结算商品数据  判断库存，库存减少，销量增加，创建订单商品信息表记录 OrderGoods
                for sku_id in sku_id_list:
                    while True:
                        # 查询该商品的最新库存
                        sku = SKU.objects.get(id=sku_id)
                        # 库存
                        sku_count = cart[sku.id]
                        origin_stock = sku.stock    # 原始库存
                        origin_sales = sku.sales

                        if origin_stock < sku_count:
                            # 回滚到保存点
                            transaction.savepoint_rollback(save_id)
                            raise serializers.ValidationError('商品%s库存不足' % sku.id)

                        # 库存减少，销量增加
                        # sku.stock -= sku_count
                        # sku.sales += sku_count
                        # sku.save()

                        new_stock = origin_stock - sku_count
                        new_sales = origin_sales + sku_count

                        # 乐观锁: update 返回受影响的行数
                        result = SKU.objects.filter(id=sku.id, stock=origin_stock).update(stock=new_stock, sales=new_sales)

                        if result == 0:
                            continue

                        order.total_count += sku_count,
                        order.total_amount += (sku.price * sku_count)

                        # 创建订单商品信息表记录 OrderGoods
                        OrderGoods.objects.create(
                            order=order,
                            sku=sku,
                            count=sku_count,
                            price=sku.price,
                        )

                        break

            order.save()
        except serializers.ValidationError as e:
            raise
        except Exception as e:
            logger.error(e)
            transaction.savepoint_rollback(save_id)
            raise
        else:
            transaction.savepoint_commit(save_id)

        # 删除购物车中已结算的商品
        pl = redis_conn.pipeline()

        # hash  勾选
        pl.hdel('cart_%s' % user.id, *redis_cart_selected)
        # set
        pl.srem('cart_selected_%s' % user.id, *redis_cart_selected)
        pl.execute()

        # 返回 OrderInfo 对象
        return order

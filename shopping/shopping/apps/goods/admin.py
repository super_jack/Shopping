from django.contrib import admin
from . import models
import xadmin

# Register your models here.

xadmin.site.register(models.Goods)
xadmin.site.register(models.GoodsCategory)
xadmin.site.register(models.GoodsChannel)
xadmin.site.register(models.GoodsSpecification)
xadmin.site.register(models.Brand)
xadmin.site.register(models.SpecificationOption)
xadmin.site.register(models.SKU)
xadmin.site.register(models.SKUImage)
xadmin.site.register(models.SKUSpecification)


# 使用 django-admin 站点模型类管理器触发 celery 异步任务
class SKUAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        obj.save()
        from shopping.celery_tasks.html.tasks import generate_static_sku_detail_html
        generate_static_sku_detail_html.delay(obj.id)


class SKUSpecificationAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        from shopping.celery_tasks.html.tasks import generate_static_sku_detail_html
        generate_static_sku_detail_html.delay(obj.sku.id)

    def delete_model(self, request, obj):
        sku_id = obj.sku.id
        obj.delete()
        from shopping.celery_tasks.html.tasks import generate_static_sku_detail_html
        generate_static_sku_detail_html.delay(sku_id)


class SKUImageAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        # obj --> SKUImage 对象   obj.sku 拿到与 obj 有关的 sku 对象
        obj.save()
        from shopping.celery_tasks.html.tasks import generate_static_sku_detail_html
        generate_static_sku_detail_html.delay(obj.sku.id)

        # 设置 SKU 默认图片
        sku = obj.sku
        if not sku.default_image_url:
            sku.default_image_url = obj.image.url
            sku.save()

    def delete_model(self, request, obj):
        sku_id = obj.sku.id
        obj.delete()
        from shopping.celery_tasks.html.tasks import generate_static_sku_detail_html
        generate_static_sku_detail_html.delay(sku_id)


# 注册对应模型类使用的模型类管理器
admin.site.register(models.Goods)
admin.site.register(models.GoodsCategory)
admin.site.register(models.GoodsChannel)
admin.site.register(models.GoodsSpecification)
admin.site.register(models.Brand)
admin.site.register(models.SpecificationOption)
admin.site.register(models.SKU, SKUAdmin)
admin.site.register(models.SKUImage, SKUImageAdmin)
admin.site.register(models.SKUSpecification, SKUSpecificationAdmin)
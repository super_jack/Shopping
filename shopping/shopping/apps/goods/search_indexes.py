from haystack import indexes

from goods.models import SKU


class SKUIndex(indexes.SearchIndex, indexes.Indexable):
    """商品 SKU 索引类"""
    # 为模型类创建索引字段, 即搜索字段
    text = indexes.CharField(document=True, use_template=True)

    # 索引类映射模型类生成索引类字段，通过 model_attr 属性指定对应模型类字段
    # 根据模型类创建的索引字段只和对应的模型类单一字段进行比较，不能跨字段做比较
    id = indexes.IntegerField(model_attr='id')
    name = indexes.CharField(model_attr='name')
    price = indexes.DecimalField(model_attr='price')
    default_image_url = indexes.CharField(model_attr='default_image_url')
    comments = indexes.IntegerField(model_attr='comments')

    def get_model(self):
        """指定创建索引的模型类对象"""
        return SKU

    def index_queryset(self, using=None):
        """指明索引查询集范围"""
        return SKU.objects.filter(is_launched=True)

import pickle, base64

from django_redis import get_redis_connection


def merge_cart_cookie_to_redis(request, user, response):
    """
    登录时合并购物车，将 cookie 中的数据合并到 redis 中
    解决合并时 redis 和 cookie 数据冲突，考虑以 cookie 数据为准
    1. 商品数量
    2. 勾选状态

    合并前:
    redis_cart = {
        '1': '20',
        '2': '2',
        '3': '5'
    }
    redis_cart_selected = set(1, 3)

    cookie_cart = {
        1: {
            'count': 10,
            'selected': False
        },
        4: {
            'count': 6,
            'selected': True
        }}

    以 cookie 为准合并到 redis 后:
    redis_cart = {
        '1': '10',
        '2': '2',
        '3': '5',
        '4': '6'
    }
    redis_cart_selected = set(3, 4)
    """
    # 获取 cookie 中的购物车数据
    cookie_cart = request.COOKIES.get('cart')  # cookie: string

    if not cookie_cart:
        return response
    # if cookie_cart:
        # 表示 cookie 中有购物车数据
        # 解析 cookie 字符串
    cart_dict = pickle.loads(base64.b64decode(cookie_cart.encode()))
    # else:
        # 表示 cookie 中没有购物车数据
        # cart_dict = {}

    # 获取 redis 中购物车中数据，hash
    redis_conn = get_redis_connection('cart')
    redis_cart = redis_conn.hgetall('cart_%s' % user.id)

    # 遍历 cookie 中的购物车数据
    cart = {}       # 购物车中最终合并返回的字典
    for sku_id, count in redis_cart.items():
        cart[int(sku_id)] = int(count)

    # 用来记录 redis 操作时，那些 sku_id 是需要勾选新增的
    redis_cart_selected_add = []

    # 用来记录 redis 操作时，那些 sku_id 是需要取消勾选删除的
    redis_cart_selected_remove = []

    for sku_id, count_selected_dict in cart_dict.items():
    # 处理商品的数量，维护在 redis 中，购物车数据数量的最终字典
          cart[sku_id] = count_selected_dict['count']

    #     处理商品的勾选状态
          if count_selected_dict['selected']:
    #           如果 cookie 指明 不勾选
                redis_cart_selected_remove.append(sku_id)
          else:
    #           如果 cookie 未指明 勾选
                redis_cart_selected_add.append(sku_id)

    if cart:      # cart = {} 为假
        # 设置 redis 中的数据     hash 和 set
        pl = redis_conn.pipeline()
        # 设置 hash
        pl.hset('cart_%s' % user.id, cart)

        # 设置 set
        pl.srem('cart_selected_%s' % user.id, *redis_cart_selected_remove)
        pl.sadd('cart_selected_%s' % user.id, *redis_cart_selected_add)
        pl.execute()

    # 清空删除 cookie
    response.delete_cookie('cart')
    return response

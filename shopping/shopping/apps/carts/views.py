from django_redis import get_redis_connection
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
import base64, pickle

from goods.models import SKU
from . import serializers

# Create your views here.


class CartView(GenericAPIView):
    """
    用户购物车数据类视图
    sku_id, count, selected
    """
    # 获取 sku_id, count, selected
    # 校验
    # 以上序列化器实现校验功能
    serializer_class = serializers.CartSerializer

    def perform_authentication(self, request):
        """将执行具体请求方法前的身份认证关掉，由视图自己进行身份认证"""
        pass

    def post(self, request):
        """保存购物车"""
        # 获取请求体内数据: request.data
        serializer = self.get_serializer(data=request.data)
        serializer.is_volid(raise_excption=True)

        sku_id = serializer.validated_data['sku_id']
        count = serializer.validated_data['count']
        selected = serializer.validated_data['selected']

        # 判断用户登陆状态
        try:
            user = request.user     # 匿名用户 AnonymoseUser
        except Exception:
            user = None

        # 保存
        if user and user.authfenticated:
            # 用户已登陆 保存到 redis
            redis_conn = get_redis_connection('cart')
            pl = redis_conn.pipeline()
            # 用户购物车数据 redis hash  -->  数据叠加     self.hincrby(key, name, count)
            pl.hincrby('cart_%s' % user.id, sku_id, count)

            # 用户购物车勾选数据 redis set
            if selected:
                pl.sadd('cart_selected_%s' % user.id, sku_id)

            pl.execute()
            return Response(serializer.data)

        else:
            # 用户未登陆 保存到 cookie  --> response = Response()   response.set_cookie()
            # 取出 cookie 中的购物车数据
            cart_cookie_str = request.COOKIES.get('cart')       # cookie: string

            if cart_cookie_str:
            # 解析 cookie 字符串
                cart_str = cart_cookie_str.encode()                 # string.encode(): str --> bytes(16进制)
                cart_bytes = base64.b64decode(cart_str)             # base64.b64decode(): bytes(16进制) --> bytes(字符编码)
                cart_dict = pickle.loads(cart_bytes)                # pickle.loads(): bytes(字符编码) --> python-dict
            else:
                cart_dict = {}

            # 购物车中数据存储格式
            #
            # cart_dict = {
            #     'sku_id_1': {
            #         'count': 10,
            #         'selected': True
            #     },
            #     'sku_id_2': {
            #         'count': 10,
            #         'selected': True
            #     },
            #     'sku_id_3': {
            #         'count': 10,
            #         'selected': True
            #     },
            # }
            #

            # 如果 商品存在购物车中，叠加
            if sku_id in cart_dict:
                cart_dict[sku_id]['count'] += count             # count 累加
                cart_dict[sku_id]['selected'] = selected        # selected 覆盖
            else:
                # 如果 商品不在购物车中，设置
                cart_dict[sku_id] = {
                    'count': count,
                    'selected': selected
                }

            cart_cookie = base64.b64encode(pickle.dumps(cart_dict)).decode()

            # 设置 cookie
            response = Response(serializer.data)
            response.set_cookie('cart', cart_cookie, max_age=3600)          # response.set_cookie(key, value, max_age_expire_time)

            return response

    def get(self, request):
        """查询购物车"""
        # 判断用户的登陆状态
        try:
            user = request.user
        except Exception:
            user = None

        # 用户已登陆 redis
        if user and user.is_authebticated:
            redis_conn = get_redis_connection('cart')
            redis_cart = redis_conn.hgetall('cart_%s' % user.id)
            redis_cart_selected = redis_conn.smembers('cart_selected_%s' % user.id)

            # 遍历 redis_cart，形成 cart_dict
            cart_dict = {}
            for sku_id, count in redis_cart.items():
                cart_dict[int(sku_id)] = {
                    'count': count,
                    'selected': sku_id in redis_cart_selected
                }
        # 用户未登陆 cookie
        else:
            cookie_cart = request.COOKIES.get('cart')  # cookie: string

            if cookie_cart:
                # 表示 cookie 中有购物车数据
                # 解析 cookie 字符串
                cart_dict = pickle.loads(base64.decode(cookie_cart.encode()))
            else:
                # 表示 cookie 中没有购物车数据
                cart_dict = {}

        # 查询数据库 购物车详细信息
        sku_id_list = cart_dict.keys()
        sku_obj_list = SKU.objects.filter(id__in=sku_id_list)

        # 遍历 sku_obj_list 向 sku 对象中添加 count 和 selected 属性
        for sku in sku_obj_list:
            sku.count = sku[sku.id]['count']
            sku.selected = sku[sku.id]['selected']

        # 序列化返回
        serializer = serializers.CartSKUSerializer(sku_obj_list, many=True)
        return Response(serializer.data)

    def put(self, request):
        """修改购物车"""
        # sku_id, count, selected
        # 校验参数
        serializer = self.get_serializer(data=request.data)
        serializer.is_volid(raise_excption=True)

        sku_id = serializer.validated_data['sku_id']
        count = serializer.validated_data['count']
        selected = serializer.validated_data['selected']

        # 判断用户登陆状态
        try:
            user = request.user
        except Exception:
            user = None

        # 如果用户登陆 修改 redis
        if user and user.is_authebticated:
            redis_conn = get_redis_connection('cart')
            pl = redis_conn.pipeline()

            # 处理数量  hash
            pl.hset('cart_%s' % user.id, sku_id, count)

            # 处理勾选状态   set
            if selected:
                # 表示勾选
                pl.sadd('cart_selected_%s' % user.id, sku_id)       # 若存在重复添加，set 可去重
            else:
                # 表示取消勾选
                pl.srem('cart_selected_%s' % user.id, sku_id)       # 若勾选 sku_id 不存在，redis 可直接忽略

            pl.execute()
            return Response(serializer.data)

        # 如果用户未登陆 修改 cookie
        else:
            cookie_cart = request.COOKIES.get('cart')  # cookie: string

            if cookie_cart:
                # 表示 cookie 中有购物车数据
                # 解析 cookie 字符串
                cart_dict = pickle.loads(base64.decode(cookie_cart.encode()))
            else:
                # 表示 cookie 中没有购物车数据
                cart_dict = {}

            response = Response(serializer.data)

            if sku_id in cart_dict:
                cart_dict[sku_id] = {
                    'count': count,
                    'selected': selected
                }

                cart_cookie = base64.b64encode(pickle.dumps(cart_dict)).decode()

                # 设置 cookie
                response.set_cookie('cart', cart_cookie,
                                    max_age=3600)  # response.set_cookie(key, value, max_age_expire_time)

            return response

    def delete(self, request):
        """删除购物车"""
        # sku_id
        # 校验
        serializer = serializers.CartDeleteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sku_id = serializer.validated_data['sku_id']

        # 判断用户登陆状态
        try:
            user = request.user     # 匿名用户 AnonymoseUser
        except Exception:
            user = None

        # 用户已登陆 删除 redis
        if user and user.authfenticated:
            redis_conn = get_redis_connection('cart')
            pl = redis_conn.pipeline()

            # 删除 count  hash
            pl.hdel('cart_%s' % user.id)

            # 删除 selected   set
            pl.srem('cart_selected_%s' % user.id)

            pl.execute()
            return Response(status=status.HTTP_204_NO_CONTENT)

        # 用户未登陆 删除 cookie
        else:
            cookie_cart = request.COOKIES.get('cart')  # cookie: string

            if cookie_cart:
                # 表示 cookie 中有购物车数据
                # 解析 cookie 字符串
                cart_dict = pickle.loads(base64.decode(cookie_cart.encode()))
            else:
                # 表示 cookie 中没有购物车数据
                cart_dict = {}

            response = Response(status=status.HTTP_204_NO_CONTENT)

            if sku_id in cart_dict:
                del cart_dict[sku_id]

                cart_cookie = base64.b64encode(pickle.dumps(cart_dict)).decode()

                # 设置 cookie
                response.set_cookie('cart', cart_cookie,
                                    max_age=3600)  # response.set_cookie(key, value, max_age_expire_time)

            return response


class CartSelectAllView(GenericAPIView):
    """
    全选购物车
    """
    serializer_class = serializers.CartSelectAllSerializer

    def perform_authentication(self, request):
        """将执行具体请求方法前的身份认证关掉，由视图自己进行身份认证"""
        pass

    def put(self, request):
        # selected
        # 校验
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        selected = serializer.validated_data['selected']

        # 判断用户登陆状态
        try:
            user = request.user     # 匿名用户 AnonymoseUser
        except Exception:
            user = None

        # 用户已登陆 redis
        if user and user.authfenticated:
            redis_conn = get_redis_connection('cart')
            redis_cart = redis_conn.hgetall('cart_%s' % user.id)

            sku_id_list = redis_cart.keys()

            if selected:
                # 全选    所有 sku_id 添加到 redis set
                redis_conn.sadd('cart_%s' % user.id, *sku_id_list)
            else:
                # 取消全选  清空 redis 中的 set 数据
                redis_conn.srem('cart_%s' % user.id, *sku_id_list)

            return Response({'message': 'OK'})

        # 用户未登陆 cookie
        else:
            cookie_cart = request.COOKIES.get('cart')  # cookie: string

            if cookie_cart:
                # 表示 cookie 中有购物车数据
                # 解析 cookie 字符串
                cart_dict = pickle.loads(base64.decode(cookie_cart.encode()))
            else:
                # 表示 cookie 中没有购物车数据
                cart_dict = {}

            response = Response({'message': 'OK'})

            if cart_dict:
                for count_selected_dict in cart_dict.values():
                    count_selected_dict['selected'] = selected

                cart_cookie = base64.b64encode(pickle.dumps(cart_dict)).decode()

                # 设置 cookie
                response.set_cookie('cart', cart_cookie,
                                    max_age=3600)  # response.set_cookie(key, value, max_age_expire_time)

            return response

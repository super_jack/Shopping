import re

from django_redis import get_redis_connection
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings

from goods.models import SKU
from shopping.celery_tasks.email.tasks import send_active_email
from users.models import User, Address


class CreateUserSerializer(serializers.ModelSerializer):
    """创建用户的序列化器"""

    password2 = serializers.CharField(label='确认密码', write_only=True)
    sms_code = serializers.CharField(label='短信验证码', write_only=True)
    allow = serializers.CharField(label='同意协议', write_only=True)
    jwt_token = serializers.CharField(label='JWT Token', read_only=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'password2', 'sms_code', 'mobile', 'allow', 'jwt_token']
        # 额外参数校验，给现有字段添加额外属性
        extra_kwargs = {
            'username': {
                'min_length': 5,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            }
        }

        def validate_mobile(self, value):
            """校验手机号"""
            if not re.match(r'^1[3-9]\d{9}$', value):
                raise serializers.ValidationError('手机号格式错误')
            return value

        def validate_allow(self, value):
            """校验用户同意协议"""
            if value != True:
                raise serializers.ValidationError('请同意用户协议')
            return value

        def validate(self, data):
            """校验两次密码"""
            if data['password'] != data['password2']:
                raise serializers.ValidationError('两次密码不一致')

            # 比较短信验证码
            redis_comm = get_redis_connection('verify_codes')
            real_sms_code = redis_comm.get('sms_%s' % data['mobile'])

            if real_sms_code is None:
                raise serializers.ValidationError('无效短信验证码')

            if data['sms_code'] != real_sms_code:
                raise serializers.ValidationError('短信验证码错误')

        def create(self, validated_data):
            """重写保存方法，增加密码加密"""

            # 移除数据模型中不存在的属性
            del validated_data['password2']
            del validated_data['sms_code']
            del validated_data['allow']

            user = User.objects.create(**validated_data)
            # user = super().create(validated_data)
            user.set_password(validated_data['password'])
            user.save()

            # 给 user 签发 JWT token
            jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
            jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)

            user.token = token

            return user


class UserDetailSerializer(serializers.ModelSerializer):
    """用户详情信息序列化器"""
    class Meta:
        model = User
        fields = ('id', 'username', 'mobile', 'email', 'email_active')


class EmailSerializer(serializers.ModelSerializer):
    """保存用户邮箱序列化器"""
    class Meta:
        model = User
        fields = ('id', 'email')        # django 有自带的邮箱格式验证，以及用户 ID 映射

    def update(self, instance, validated_data):
        """
        :param instance: EmailView 类视图 返回的 user 对象
        :param validated_data:
        :return:
        """
        email = validated_data['email']

        # 保存 email 信息
        instance.email = email
        instance.save()

        # 生成激活链接
        verify_url = instance.generate_verify_email_url()

        # 触发 celery 异步任务，发送邮件给用户
        send_active_email.delay(email, verify_url)

        return instance


class UserAddressSerializer(serializers.ModelSerializer):
    """用户地址序列化器"""
    province = serializers.StringRelatedField(read_only=True)
    city = serializers.StringRelatedField(read_only=True)
    district = serializers.StringRelatedField(read_only=True)
    province_id = serializers.IntegerField(label='省ID', required=True)
    city_id = serializers.IntegerField(label='市ID', required=True)
    district_id = serializers.IntegerField(label='区ID', required=True)

    class Meta:
        model = Address
        exclude = ('user', 'is_delete', 'create_time', 'update_time')

    def validate_mobile(self, value):
        """验证手机号"""
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号格式错误')
        return value

    def create(self, validated_data):
        """保存"""
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)


class AddressTitleSerializer(serializers.ModelSerializer):
    """用户地址标题序列化器"""

    class Meta:
        model = Address
        fields = ('title',)


class AddUserBrowsingHistorySerializer(serializers.Serializer):
    """保存用户浏览历史记录序列化器"""
    sku_id = serializers.IntegerField(label='商品SKU编号', min_value=1, required=True, write_only=True)

    def validate_sku_id(self, value):
        """校验商品 sku_id"""
        try:
            SKU.objects.get(id=value)
        except SKU.DoesNotExist:
            raise serializers.ValidationError('该商品不存在')
        return value

    def create(self, validated_data):
        """保存"""
        # sku_id
        sku_id = validated_data['sku_id']

        # user_id  --> key
        user = self.context['request'].user
        redis_key = 'history_%s' % user.id

        # redis 链接，按照对应值类型设置对应值
        redis_conn = get_redis_connection('history')
        pl = redis_conn.pipeline()

        # 去重
        # lrem(key, count, value)
        pl.lrem(redis_key, 0, sku_id)

        # 保存
        # lpush(key, value) 从左边添加，从左边取
        # rpush(key, value) 从右边添加，从右边取
        pl.lpush(redis_key, sku_id)

        # 截断，保存新的，删除旧的
        # ltrim(key, start, stop)
        pl.ltrim(redis_key, 0, 10)

        # redis 管道执行
        pl.execute()

        # 如果有模型类对象，就返回对应的模型类对象
        # 如果没有模型类对象，就返回对应的验证数据字典即可
        return validated_data


class SKUSerializer(serializers.ModelSerializer):
    """序列化渲染返回用户浏览历史记录"""
    class Meta:
        model = SKU
        fields = ('id', 'name', 'price', 'default_image_url', 'comments')

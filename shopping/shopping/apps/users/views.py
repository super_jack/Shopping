from django.shortcuts import render

# Create your views here.
from django_redis import get_redis_connection
from rest_framework.decorators import action
from rest_framework.viewsets import GenericViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import mixins, generics, status
from rest_framework.generics import UpdateAPIView, CreateAPIView
from rest_framework_jwt.views import ObtainJSONWebToken
from carts.utils import merge_cart_cookie_to_redis
from goods.models import SKU
from users import serializers
from users.models import User
from users.serializers import CreateUserSerializer, UserDetailSerializer, EmailSerializer, UserAddressSerializer


# url(r'^users/$', views.UserView.as_view())
class UserView(generics.CreateAPIView):      # 提供 post 方法
    """
    用户注册
    传入参数: username, password, password2, sms_code, mobile, allow
    """
    serializer_class = CreateUserSerializer

    # 提供反序列化 校验保存，以及序列化 返回的功能
    # CreateAPIView 提供 post 方法，所以以下可省略
    # def post(self):
    #     pass

    # CreateAPIView = CreateModelMixin + GenericAPIView

    # class CreateAPIView(mixins.CreateModelMixin, GenericAPIView):
    #     """
    #     Concrete view for creating a model instance.
    #     """
    #
    #     def post(self, request, *args, **kwargs):
    #         return self.create(request, *args, **kwargs)


# url(r'^usernames/(?P<username>\w{5-20})/count/$', views.UsernameCountView.as_view())
class UsernameCountView(APIView):
    """用户名数量"""
    def get(self, request, username):
        """获取指定用户名数量"""
        count = User.objects.filter(username=username).count()

        data = {
            'username': username,
            'count': count
        }

        return Response(data)


# url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCountView.as_view())
class MobileCountView(APIView):
    """手机号数量"""
    def get(self, request, mobile):
        """获取指定手机号数量"""
        count = User.objects.filter(mobile=mobile).count()

        data = {
            'mobile': mobile,
            'count': count
        }

        return Response(data)


# GET /user/
# url(r'^user/$', views.UserDetailView.as_view())
class UserDetailView(generics.RetrieveAPIView):
    """用户基本信息"""
    serializer_class = UserDetailSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        """返回当前请求的用户对象"""
        # 在类视图对象中，可以通过类视图对象的属性获取 request
        # 在 Django 的请求对象 request 中，user 属性代表当前请求的用户
        return self.request.user


# PUT /email/
# url(r'^email/$', views.EmailView.as_view())
class EmailView(UpdateAPIView):
    """保存用户邮箱信息"""
    permission_classes = [IsAuthenticated]
    serializer_class = EmailSerializer

    def get_object(self):
        return self.request.user


# url(r'^emails/verification/$', views.VerifyEmailView.as_view())
class VerifyEmailView(APIView):
    """邮箱验证"""
    def get(self, request):
        # 获取 token
        token = request.query_params('token')

        if not token:
            return Response({'message': '链接信息无效'}, status=status.HTTP_400_BAD_REQUEST)

        # 验证 token
        user = User.check_verify_email_token(token)
        if user is None:
            return Response({'message': '链接信息无效'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            user.email_active = True
            user.save()
            return Response({'message': 'OK'})


# 用户收货地址    ModelViewSet 提供 5 种行为
# 新增        POST /addresses/
# 修改        PUT /addresses/<pk>/
# 查询        GET /addresses/
# 删除        DELETE /addresses/<pk>/
# 设置默认地址 PUT /addresses/<pk>/status/
# 设置地址标题 PUT /addresses/<pk>/title/

class AddressViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin, GenericViewSet):
    """用户地址新增与修改"""
    serializer_class = UserAddressSerializer
    pagination_class = [IsAuthenticated]

    def get_queryset(self):
        return self.request.user.addresses.filter(is_delete=False)

    # GET /addresses/
    def list(self, request, *args, **kwargs):
        """用户地址列表数据"""
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        user = self.request.user
        return Response({
            'user_id': user.id,
            'default_address_id': user.default_address_id,
            'limit': 20,
            'addresses': serializer.data,
        })

    # POST /addresses/
    def create(self, request, *args, **kwargs):
        """保存用户地址数据"""
        count = request.user.addresses.count()
        if count >= 20:
            return Response({'message': '保存地址数据已达到上限'}, status=status.HTTP_400_BAD_REQUEST)

        return super().create(request, *args, **kwargs)

    # DELETE /addresses/<pk>/
    def destroy(self, request, *args, **kwargs):
        """删除用户地址数据"""
        address = self.get_object()
        address.is_delete = True
        address.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

    # PUT /addresses/<pk>/status/
    @action(methods=['put'], detail=True)
    def status(self, request, pk=None):
        """设置默认地址"""
        address = self.get_object()
        request.user.default_address = address
        request.user.save()

        return Response({'message': 'OK'}, status=status.HTTP_200_OK)

    # PUT /addresses/<pk>/title/
    @action(methods=['put'], detail=True)
    def title(self, request, pk=None):
        """修改标题"""
        address = self.get_object()
        serializer = serializers.AddressTitleSerializer(instance=address, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data)


class UserBrowsingHistoryView(CreateAPIView):
    """
    查询和保存用户浏览历史记录
    """
    serializer_class = serializers.AddUserBrowsingHistorySerializer
    permission_classes = [IsAuthenticated]

    def get(self, request):
        # user_id
        user_id = request.user.id
        # 查询 redis list -->  redis 保存 sku_id
        # lrange(key, start, stop)
        redis_conn = get_redis_connection('history')
        sku_id_list = redis_conn.lrange('history_%s' % user_id, 0, 10)

        # 查询数据库     mysql 根据 redis 中的 sku_id 查询对应的 sku 即可
        skus = []
        for sku_id in sku_id_list:
            sku = SKU.objects.get(id=sku_id)
            skus.append(sku)

        # 序列化返回
        serializer = serializers.SKUSerializer(skus, many=True)

        return Response(serializer.data)


class UserAuthorizeView(ObtainJSONWebToken):
    """
    用户登陆认证视图    -->  登陆成功时，调用函数 合并购物车
    """
    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)

        # 如果用户登陆成功，合并购物车
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid:
            user = serializer.validated_data['user']
            response = merge_cart_cookie_to_redis(request, user, response)

        return response

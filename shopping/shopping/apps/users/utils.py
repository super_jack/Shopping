import re
from .models import User
from django.contrib.auth.backends import ModelBackend


def jwt_response_payload_handler(token, user=None, request=None):
    """自定义 JWT 认证成功后返回的数据"""
    return {
        'token': token,
        'user_id': user.id,
        'username': user.username
    }


def get_user_by_account(account):
    """根据账号获取用户对象"""
    try:
        if re.match(r'^1[3-9]\d{9}$', account):
            user = User.objects.get(mobile=account)
        else:
            user = User.objects.get(username=account)
    except User.DoesNotExist:
        return None
    else:
        return user


class UsernameMobileAuthBackend(ModelBackend):
    """自定义用户名手机号登陆认证"""
    def authenticate(self, request, username=None, password=None, **kwargs):
        # 获取 user 对象
        user = get_user_by_account()
        # 如果用户存在，校验密码
        if user is not None and user.check_password(password):
            return user
        else:
            return None

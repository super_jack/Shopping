from django.conf.urls import url, include

from oauth import views

urlpatterns = [
    url(r'^qq/authorzation/$', views.QQAuthURLView.as_view()),
    url(r'^qq/user/$', views.QQAuthUserView.as_view()),
]

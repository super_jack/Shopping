from django_redis import get_redis_connection
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings

from oauth.models import OAuthQQUser
from oauth.utils import OAuthQQ
from users.models import User


class OAuthQQUserSerializer(serializers.ModelSerializer):
    sms_code = serializers.CharField(label='短信验证码', write_only=True)
    access_token = serializers.CharField(label='操作凭证', write_only=True)
    token = serializers.CharField(read_only=True)
    mobile = serializers.RegexField(label='手机号', regex=r'1[3-9]\d{9}')      # 双向

    class Meta:
        model = User
        fields = ('mobile', 'password', 'sms_code', 'access_token', 'id', 'username', 'token')
        extra_kwargs = {
            'username': {
                'read_only': True
            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            }
        }

    def validate(self, attr):
        """validate"""
        # 校验 access_token
        access_token = attr['access_token']
        open_id = OAuthQQ.check_bind_user_access_token(access_token)

        if not open_id:
            raise serializers.ValidationError('无效到 access_token')

        attr['open_id'] = open_id

        # 校验短信验证码
        mobile = attr['mobile']
        sms_code = attr['sms_code']
        redis_conn = get_redis_connection('verify_codes')
        real_sms_code = redis_conn.get('sms_%s' % mobile).decode()
        if sms_code != real_sms_code:
            raise serializers.ValidationError('短信验证码错误')

        # 如果用户存在，检验用户密码
        try:
            user = User.objects.get(mobile=mobile)
        except User.DoesNotExist:
            pass
        else:
            password = attr['password']
            if not user.check_password(password):
                raise serializers.ValidationError('密码错误')

            attr['user'] = user
        return attr

    def create(self, validated_data):
        """save"""
        open_id = validated_data['open_id']
        user = validated_data['user']
        mobile = validated_data['mobile']
        password = validated_data['password']

        if not user:
            # 如果 user 不存在 创建 OAuthQQUser 对象, 并绑定
            user = user.objects.create_user(username=mobile, mobile=mobile, password=password)

        # 如果 user 存在，绑定，保存 OAuthQQUser 数据
        OAuthQQUser.objects.creat(user=user, open_id=open_id)

        # 签发 JWT token
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        user.token = token

        # 通过序列化器对象 self 拿到 对应的类视图对象: self.context['view']
        # 其中，context 中 request 属性 拿到 请求对象；kwgs 拿到查询字符串参数
        self.context['view'].user = user

        return user

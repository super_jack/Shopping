from django.shortcuts import render
from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings
from carts.utils import merge_cart_cookie_to_redis
from oauth.serializers import OAuthQQUserSerializer
from .exceptions import OAuthQQAPIError
from oauth.utils import OAuthQQ
from oauth.models import OAuthQQUser
import logging

# Create your views here.
logger = logging.getLogger('django')


# url(r'^qq/authorzation/$', views.QQAuthURLView.as_view())
class QQAuthURLView(APIView):
    """获取QQ登陆URL"""

    def get(self, request):
        # 获取 next 参数
        next = request.query_params.get('next')
        # 拼接 QQ 登陆 URL
        oauth_qq = OAuthQQ(scope=next)
        login_url = oauth_qq.get_login_url()
        # 返回到客户端
        return Response({'login_url': login_url}, status=200)


class QQAuthUserView(CreateAPIView):
    """获取QQ用户openid"""
    serializer_class = OAuthQQUserSerializer

    def get(self, request):
        # 获取 code 参数
        code = request.query_params.get('code')
        if not code:
            return Response({'message': 'not code!'}, status=status.HTTP_400_BAD_REQUEST)

        # 凭借 code 请求 access_token
        oauth = OAuthQQ()

        try:
            access_token = oauth.get_access_token(code)
        except OAuthQQAPIError as e:
            logger.error(e)
            return Response({'message': '访问 access_token 失败！'}, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        try:
            # 凭借 access_token 请求 open_id
            open_id = oauth.get_open_id(access_token)
        except OAuthQQAPIError as e:
            logger.error(e)
            return Response({'message': '获取 open_id 接口失败！'}, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        try:
            # 查询数据库是否存在该用户 open_id，判断该用户是否第一次 QQ 登陆
            oauth_user = OAuthQQUser.objects.get(open_id=open_id)
        except OAuthQQUser.DoesNotExit:
            # 代表该用户第一次使用 QQ 登陆，返回校验该用户身份的 access_token
            oauth.generate_bind_access_token(open_id)
            return Response({'access_token': access_token})
        else:
            # 如果不是首次登陆，处理 openID 并返回
            # 给 user 签发 JWT token
            jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
            jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

            user = oauth_user.user
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)

            response = Response({
                'username': user.username,
                'user_id': user.id,
                'token':token
            })

            # QQ 登陆成功后，签发 JWT 之前，合并购物车
            response = merge_cart_cookie_to_redis(request, user, response)

            return response

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)

        # 合并购物车
        user = self.user
        response = merge_cart_cookie_to_redis(request, user, response)

        return response
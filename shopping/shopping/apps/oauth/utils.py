import json
from itsdangerous import TimedJSONWebSignatureSerializer as TJWSSerializer, BadData
from django.conf import settings
from urllib.parse import urlencode, parse_qs
from urllib.request import urlopen
from .exceptions import OAuthQQAPIError
import logging

logger = logging.getLogger('django')


class OAuthQQ(object):
    """QQ 认证辅助工具类"""

    def __init__(self, client_id=None, redirect_uri=None, scope=None, client_secret=None):
        self.client_id = client_id or settings.QQ_CLIENT_ID
        self.redirect_uri = redirect_uri or settings.QQ_REDIEECT_URI
        self.scope = scope or settings.QQ_SCOPE
        self.client_secret = client_secret or settings.QQ_CLIENT_SECRET

    def get_login_url(self):
        """拼接 QQ 网址"""

        url = 'https://graph.qq.com/oauth2.0/authorize?'

        params = {
            'response_type': 'code',
            'client_id': self.client_id,
            'redirect_uri': self.redirect_uri,
            'scope': self.scope
        }

        query_params = urlencode(params)
        # ?response_type=code&client_id=self.client_id&redirect_uri=self.redirect_uri&scope=self.scope

        url += query_params

        return url

    def get_access_token(self, code):
        """获取 access_token"""
        url = 'https://graph.qq.com/oauth2.0/token?'

        params = {
            'grant_type': 'authorization_code',
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'code': code,
            'redirect_uri': self.redirect_uri,
        }

        url += urlencode(params)
        try:
            # 发送请求  get
            resp = urlopen(url)
            # 读取响应体数据，bytes.decode() --> string
            resp_data = resp.read().decode()
            # 解析 access_token   urllib.parse.parse_qs
            resp_dict = parse_qs(resp_data)
        except Exception as e:
            logger.error(e)
            raise OAuthQQAPIError
        else:
            # 获取 access_token 字符串值，其中，resp_dict 中包括 access_token, expire_in, refresh_token
            access_token = resp_dict['access_token']
            return access_token[0]    # list

    def get_open_id(self, access_token):
        """获取用户 open_id"""
        try:
            url = 'https://graph.qq.com/oauth2.0/me?access_token=' + access_token

            # 返回 Json 字符串 使用字符串切片取出字典段: callback( {"client_id":"YOUR_APPID","openid":"YOUR_OPENID"} );
            resp_data = urlopen(url)[10:-3]
            resp_data_dict = json.loads(resp_data)
        except Exception as e:
            logger.error(e)
            raise OAuthQQAPIError
        else:
            open_id = resp_data_dict['openid']
            return open_id

    def generate_bind_access_token(self, open_id):
        """绑定首次登陆用户, 返回 access_token"""
        # itsdangerous.TimedJSONWebSignatureSerializer(secret_key, expire_time)
        # 可以使用 dumps() 将用户信息以字典形式，转换成 JWT Token, 返回 bytes 类型;
        # 也可以使用 loads() 校验 token, 若校验失败，会抛出 itsdangerous.BadData 异常

        serializer = TJWSSerializer(settings.SECRET_KEY, expires_in=3600)  # param: secret_key expire
        token = serializer.dumps({'open_id': open_id})          # return bytes
        return token.decode()       # 此 token 就是 access_token

    # @classmethod 类方法中存在操作类属性，没有操作实例属性，且对象为 cls
    # @staticmethod 中既没有操作实例属性，也没有操作到类属性，即为静态方法
    # 操作对象属性，即为对象方法，只有对象可以调用
    @staticmethod
    def check_bind_user_access_token(self, access_token):
        """校验用户 access_token"""
        serializer = TJWSSerializer(settings.SECRET_KEY, 3600)
        try:
            check_token_data = serializer.loads(access_token)
        except BadData:
            return None
        else:
            return check_token_data['open_id']

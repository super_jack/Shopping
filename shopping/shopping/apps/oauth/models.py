from django.db import models

# Create your models here.
from shopping.utils.models import BaseModel


class OAuthQQUser(BaseModel):
    """实现QQ登陆"""
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, verbose_name='用户')
    open_id = models.CharField(max_length=64, verbose_name='open_id', db_index=True)

    class Meta:
        db_table = 'tb_oauth_qq'
        verbose_name = 'QQ登陆用户信息'
        verbose_name_plural = verbose_name


# day 01

# ## 1/ 框架学习方法
# ## 2/ Django 学习技术点列举

# ==================================================================================================================== #
# day 02

# ## 1/ 接口设计思想
# 请求方式，请求路径，参数和返回值

# ## 2/ cors 跨域请求
# Access-Control-Allow-Origin   跨域请求
# 解决跨域请求    CORS
# 需要安装 django-cors-headers 扩展包
# 注册扩展应用，中间件，以及 CORS 跨域请求白名单，允许跨域请求携带 cookie

# ## 3/ redis pipeline 管道
# 多次 redis 操作，放进 redis pipeline 管道对象中，可以减少网络通讯次数，最后 execute 一次执行

# ## 4/ celery 异步任务
# celery 分为 3 个部分: 客户端 client使用方（Django），任务队列 broker（Redis），以及任务执行方 worker
# 经过配置，Django 将耗时任务放到 redis 中，redis 数据库充当任务队列 broker，在 任务执行方 worker 定义任务，
# 并装饰上 celery 对象 @celery_object.task(name=task_name) 供客户端调用；celery 应用目录大致分为 main,
# config, 以及 tasks，其中: config 用于配置 redis 数据库，指定任务队列存放位置，main 用于实例化 celery 对象，
# 加载 config 配置文件，指定自动识别任务 autodiscover_tasks(),传入一个任务名列表即可；在 Django 中还需要加载
# 项目配置目录，可以独立使用，celery 是独立于 Django 的架构。最后，可以使用 celery A celery_tasks.main worker -l info
# 用于启动 celery 异步任务，客户端使用 delay() 即可实现任务调用。

# ## 5/ 通过序列化器对象获取到类视图中的属性值
# 序列化器 context  --  view format request
# 类视图   kwargs  --  属性字典
# 序列化器对象 self.context['view'] 拿到类视图对象
# 再通过类视图对象 self.context['view'].kwargs['mobile'] 拿到请求路由中的 queryparam 查询字符串参数值

# ## 6/ 类视图 CreateModelMixin 中 create() 的重写，以及序列化器字段的方向

# ## 7/ DRF 框架串讲
# 序列化器: 校验数据和返回数据
# 1/ 序列化: 查询数据库，并提供数据格式转换，返回到前端
# 2/ 反序列化: 接收前端请求参数，提供校验并保存到数据库

# 类视图:
# 1/ APIView 获取 Request 对象，返回 Response 对象；提供认证，权限和限流操作
# 2/ Generic 和 mixin 提供数据库的查询和序列化功能，以及集成操作

# ## 视图集: 将增删改查操作集成到一个类中，即为类视图集合

# ==================================================================================================================== #
# day 03

# ## 1/ JWT 机制
# JWT 组成:
# header 头部 声明 JWT 类型和加密的算法
# {
#     'typ': 'JWT',
#     'alg': 'HS256'
# }
# payload 载荷 包含请求的具体数据: iss=jwt签发者，sub=jwt面向的用户，aud=jwt接收方，exp=jwt过期时间，必须大于签发时间，
# nbf=jwt过期时间点，iat=jwt签发时间，以及 jti=jwt唯一身份标示，用作一次性 token，规避重放攻击。
# {
#     'sub': '1234567890',
#     'name': 'John Doe',
#     'admin': 'true'
# }
# signature 签名  最关键  包含 base64 加密后的 header 和 payload，以及后端 secret 加密字符串
# 将加密后的 header，payload 和 signature 3 部分用(.)分隔，组成最终的 JWT。
#
# 前端发出请求，后端签发 JWT 给前端，客户端再次发出请求，携带 JWT；后端取出 JWT 的 header 和 payload 加上自身的 secret 经过 base64
# 加密生成字符串，和该 JWT 的 signature 签名部分做对比，若相等，则验证成功，否则，验证失败。
#
# 不应该在 payload 中存放敏感信息，因为 payload 中的信息，客户端可以解密出来；应该保护好 secret ，后端的 secret 是验证 JWT 的关键
#
# 前端浏览器存储有 localStorage，sessionSorage，以及 cookie，可以将 JWT Token 存放在 SessionStorage 中即可

# ## 2/ 第三方登陆 QQ 登陆     callback?code=xxx&state=xxx
# 1/ 用户点击第三方登陆按钮，携带 next 参数到本地后端服务器，服务器拼接好用于用户 QQ 登陆的 URL，并返回给客户端；
# 2/ 用户携带拼接的 URL 访问 QQ 服务器，QQ 将用户重定向到服务器的 callback(回调) 页面，并携带授权 code 和 state(next) 参数；
# 3/ 客户端携带 code 参数向 QQ 服务器请求 access_token，QQ 服务器向用户返回 access_token (令牌)；--> 验证用户身份  OAuth 开放认证系统
# 4/ 客户端携带 access_token 参数向 QQ 服务器请求 待登陆用户的 open_id,QQ 服务器向客户端返回该用户的 open_id (用户的唯一身份标识符)；
# 5/ 判断该用户是否第一次登陆，即查询数据库有没有记录该用户的 open_id；
# 6/ 若该用户不是第一次 QQ 登陆，则登陆成功，返回 JWT-Token，用户跳转到 state(next) 指明到页面；
# 7/ 若该用户是第一次 QQ 登陆，则生成绑定该用户身份的 access_token 并返回；
# 8/ 客户端携带手机号，密码，短信验证码，access_token 请求本地服务器绑定 QQ 用户身份；
# 9/ 如果本地服务器中存在用户数据，直接绑定；如果不存在，则创建用户并绑定身份；
# 10/ 向客户端返回登陆成功的 JWT Token，用户跳转到 state(next) 指定的页面。

# ## 3/ 面向对象 创建类的思考方式   使用工具类
# 根据实际需求，创建类，填充方法即可
# 有封装的思想，并允许用户自定义

# ## 4/ itsdangerous 处理 openID，生成临时身份令牌
# serializer = itsdangerous.TimedJSONWebSignatureSerializer(secret, expires)
# serializer.dumps({'open_id': open_id}), 生成 access_token
# serializer.loads(access_token)    返回一个字典

# ## 5/ 对象方法，类方法和静态方法的比较
# @classmethod 类方法中存在操作类属性，没有操作实例属性，且对象为 cls
# @staticmethod 中既没有操作实例属性，也没有操作到类属性，即为静态方法
# 操作对象属性，即为对象方法，只有对象可以调用

# ## 6/ python 标准库 urllib
# parse_qs 解析查询字符串，返回一个字典
# urlencode 将参数字典转换成查询字符串，返回一个查询字符串
# urlopen 本地服务器发起请求
# urlopen(url).read() 获取内容，返回一个 bytes 对象
# urlopen(url).read().decode()，解析成一个 string 对象

# ## 7/ python Json 对象，Json 字符串
# Json 字符串: "{'name': 'jack', 'age': 23}"
# Json 对象: {'name': 'jack', 'age': 23}
#
# python 3 字符串分为 string 和 bytes
# string.encode('utf-8') --> bytes
# bytes.decode('utf-8') ---> string
#
# python 2 字符串分为 string 与 unicode
# unicode.encode() ---> string
# string.decode()  ---> unicode

# ==================================================================================================================== #
# day 04

# ## 1/ 使用 drf 发送邮件以及生成验证链接的流程  --->  Django 自带的邮件发送功能
#
#

# ## 2/ 行政区划自关联模型字段嵌套序列化
#
#

# ## 3/ Django 缓存机制
#
#

# ## 4/ 用户地址管理逻辑
#
#

# ==================================================================================================================== #
# day 05

# ## 1/ FastDFS 快速分布式文件存储系统     集群
# Tracker server: 负载均衡和调度追踪
# Storage server: 真实文件存储服务器

# 横向: 分组，分卷；多个组一起存放了所有数据，每个组只保存了全部数据中的一个部分  扩展方便；
# 纵向: 同步，备份；每台存储服务器保持实时通信，确保数据的同步更新和及时备份；

# 文件上传流程: 客户端询问 Tracker，需要将文件上传到哪台 Storage；然后客户端将文件交给 Tracker 指定的 Storage 处理: 生成该文件的
# file_id，以及将文件内容写入磁盘；最后将 file_id 返回到客户端，即完成文件存储。用户在访问文件时，直接访问 Storage，此时，Storage
# 会配合 Nginx 代理服务器，向客户端提供静态文件的访问服务。

# ## 2/ Docker 虚拟化容器技术  使用 docker 部署 FastDFS
# Docker容器: 体积小，速度快，集成度高，共用独立功能
# VM 虚拟机: 容量大，启动慢，集成度低，复用功能少

# 镜像: 构建 docker 的应用程序 静态文件
# 容器: 正在运行的镜像，活动的应用进程就是容器
# Registry(docker-hub): docker 镜像仓库
# docker C/S 模型:
# docker 的安装与配置，先安装 VM 虚拟机，在 CentOS(Linux) 中安装，性能会好些
# docker 镜像(image) 与 容器(container) 基本命令

# ## 3/ 使用 docker 容器部署 FastDFS
# FastDFS 与 python 客户端安装: https://github.com/jefforeilly/fdfs_client-py
# pip install mutagen/requests 安装 FastDFS 依赖包   官方提供 client.conf 配置文件，修改其中 日志存放路径 base_path，Tracker server 22122

# ## 4/ 富文本编辑器 CKEditor
# ckeditor.fields.RichTextField: 不支持上传文件的富文本字段；
# ckeditor_uploader.fields.RichTextUploadingField: 支持上传文件的富文本字段；
# 注册 CKEditor 和 ckeditor_uploader 应用；
# 配置富文本编辑器，及设置上传图片保存路径；
# 最后将富文本编辑器的上传模块注册到总路由，即可在模型类中设置富文本格式字段。

# ## 5/ 使用 CKEditor 对接 FastDFS 的 bug 解决
# 在前端提供一个输入框，用户在输入数据时可以进行编辑格式，CKEditor 富文本编辑器可以
# 快速的将用户编辑的格式文本生成对应的 html 文件，并将其放到数据库中；

# ## 6/ 数据库表的设计思想: 分析需要那些表，表与表之间的关系
#
#

# ==================================================================================================================== #
# day 06

# ## 1/ 页面静态化: 数据库查询及渲染模版，并写入静态文件，然后保存即可
# 1. 定时任务(首页):  (任务时间间隔，执行定时任务的函数名路径，'>> + 日志输出文件绝对路径')

# 任务时间间隔格式
# * * * * *
# 分时日月周

# M: 分钟 (0-59)  每分钟 使用 * 或 */1 表示
# H: 小时 (0-23)  0 表示 0 点
# D: 每天 (1-31)
# m: 每月 (1-12)
# d: 一星期内的每天  (0-6, 0为星期天)

# */5 * * * *   每间隔 5 分钟执行一次
# 5 * * * *     每月每周每天每小时第 5 分钟执行一次

# 2. 操作定时任务
# 操作系统定时任务添加，显示和移除: python manager.py crontab add / show / remove
# 页面静态化，其实就是调用操作系统中的 crontab 模块进行添加执行的定时任务

# 3. 使用时刷新: 修改数据的时候，生成保存静态化页面（商品详情页）

# ## 2/ 解决 Vue 与 Django 模版语法冲突
# 在 Vue.js 文件中声明 delimiters: ['[[', ']]'], 即可将默认的 '{{' 和 '}}' 替换，且效果一致
# 而 Django 的模版语法保持原来的 '{{' 和 '}}' 不变即可消除语法冲突

# ## 3/ Django-admin 模型类管理器触发 celery 异步任务   继承自 admin.ModelAdmin
# save_model: 在 Django-admin 站点模型中操作 保存
# delete_model: 在 Django-admin 站点模型中操作 删除

# ## 4/ 商品详情页使用 admin 模型类管理器触发 celery 异步任务 实现页面静态化
# 1. 商品详情页中，不是所有的数据都需要页面静态化，可以选择部分数据
# 2. 页面静态化的触发时机，在 admin 站点中修改数据时，生成静态化页面
# 3. 将详情页页面静态化定义成异步任务，在 admin 站点模型类管理器类中触发

# ## 5/ redis 值存储设计     5 种值类型的区别
# 1. string: 单值
# 2. list:   有序，多值
# 3. hash:   键值对，一个键对应一个值(string)
# 4. set:    去重，无序
# 5. zset:   去重，根据值的权重值，设置值的顺序

# ## 6/ 用户浏览历史记录的查看和保存
#
#

# ==================================================================================================================== #
# day 07

# ## 1/ elasticsearch 搜索引擎      修改配置文件 config/elasticsearch.yml  -->  [host:localhost, port:9200]
# 1. 使用 docker 部署 elasticsearch   -->   镜像名称:版本号

# 2. 使用 haystack 完成 Django 与 elasticsearch 的对接  -->  haystack 作用等同于 ORM，只需要配置使用的搜索引擎即可，不关心具体实现
# ORM 调用数据库 Mysql，而 haystack 调用搜索引擎 elasticsearch   只是一个调用的捷克，而不关心具体的操作

# 3. 创建索引类，继承于 indexes.SearchIndex 和 indexes.Indexable      写在当前模块的 search_indexex 文件中
# 索引类的作用: 明确在搜索引擎中索引数据包含那些字段，同样: 字段也会作为前端进行检索查询时关键词的参数名

# 4. 创建索引模版文件   同 Django 模版语法   {{ object.name }}   其中，object 就指当前创建索引类的模型类对象
# 索引模版文件命名规范    创建索引的模型类名称_索引类生成索引字段的名称.txt

# 5. 使用 python manage.py rebuild_index 为模型类创建索引，生成当前模块的索引文件

# 6. 使用 haystack 中提供的 HaystackViewSet 视图集实现搜索即可

# ## 2/ 商品模块总结
# 1. 首页
# 2. 列表页
# 3. 详情页
# 4. 搜索结果页
# 5. 数据库表的设计思想
# 6. FastDFS 分布式文件存储，便于扩容
# 7. 使用 docker 进行快速部署实现
# 8. CKEditor 富文本编辑器的嵌入
# 9. 页面静态化 (首页设置定时任务和调用 celery 异步任务处理详情页面静态化)
# 10. 列表页面的快速排序和分页显示
# 11. 商品搜索，haystack 对接 elasticsearch

# ## 3/ 购物车模块需求分析
# 1. 在用户登陆与未登陆状态下，都可以保存用户的购物车数据
# 2. 用户可以对购物车数据进行增，删，改，查
# 3. 用户对于购物车数据的勾选状态也需要保存，在订单结算页面会使用到勾选数据
# 4. 用户登陆时，合并 cookie 中的购物车数据到 redis 中

# ## 4/ Pickle 与 Json 的区别
# Pickle: 效率高，同种语言内的数据转换    pickle.dumps()   python --> bytes      pickle.loads()   bytes --> python
# Json: 效率比 pickle 低，跨语言间的数据转换      json.dumps()     python --> json     json.loads()     json --> python

# ## 5/ base64 字符编码
# cookie:   string --> python-type
# encode():  string --> bytes(16进制)
# base64.b64decode():  bytes(16进制) --> bytes(字符编码)
# pickle.loads():  bytes(字符编码) --> hash 或 python-type

# cookie:   python --> string
# pickle.dumps():  hash 或 python-type --> bytes(字符编码)
# base64.b64encode():  bytes(字符编码) --> bytes(16进制)
# decode():  bytes(16进制) --> string

# ## 6/ 保存购物车数据
#
#

# ==================================================================================================================== #
# day 08

# ## 1/ 查询购物车数据
#
#

# ## 2/ 幂等性
# 1/ 传递增量: 结果与请求的次数有关，结果受请求次数影响   ---> 非幂等
# 2/ 传递结果: 结果与请求的次数无关，结果跟设置的值有关   ---> 幂等
# 幂等: 无论用户请求多少次，最终返回的结果都相同

# ## 3/ 修改购物车数据
#
#

# ## 4/ 删除购物车数据
#
#

# ## 5/ 全选购物车数据
#
#

# ## 6/ 登录时 合并购物车数据     用户登陆认证，以及第三方登陆(QQ登陆)
#
#

# ==================================================================================================================== #
# day 09

# ## 1/ 提交，保存订单
#
#

# ## 2/ 乐观锁，悲观锁和任务队列
# 1/ 悲观锁: 数据库中真实存在，使用数据库为记录加上锁，锁住别人无法操作
# 查询语句后面加上 for update，当前事务结束之后将锁释放      --> 死锁
# Django: selected_for_update()

# 2/ 乐观锁: 不是真实存在的，只在更新的时候判断此时库存是否是之前查询出的库存
# 表示没人修改，可以更新库存，否则表示别人抢过资源，不再执行库存更新

# 3/ 任务队列 依次执行  celery


# ## 3/ 事务隔离级别
# 1. Serializable: 串行化            事务间依次执行
# 2. Repeatable read: 可重复读       事务间各自隔离
# 3. Read committed: 读取已提交      事务间只能看到已提交数据
# 4. Read uncommitted: 读取未提交    事务间可以看到未提交数据

# ==================================================================================================================== #
# day 10

# ## 1/ 接入支付宝
# params:
# order_id, 订单编号
# total_amount, 总金额
# return_url, 重定向网址
# notify_url，通知网址
# sign，签名   公钥私钥验证签名
# RSA 密钥

# 1. 用户点击支付，通过 Django 程序请求支付宝，支付宝携带参数返回，经过 Django 拼接，并将其返回给用户
# 2.






















